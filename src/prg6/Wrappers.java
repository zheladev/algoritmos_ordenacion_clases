/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

import java.util.ArrayList;

/**
 *
 * @author Zhel-PC
 */
public class Wrappers {
    public static void main(String [] args) {
        final int n = 5;
        Integer nWrapper = new Integer(n);
        
        ArrayList<Integer> al = new ArrayList<>();
    
        String numeroString = "23456";
        int numeroInt = Integer.valueOf(numeroString);
        int numeroInt2 = Integer.parseInt(numeroString);
        
        //paso de variable por valor
        System.out.println(n);
        sumar(n);
        System.out.println(n);
        
        System.out.println("REF\n"+nWrapper);
        //por referencia
        sumarRef(nWrapper);
        System.out.println(nWrapper);
    }
    
    public static void sumar(int n) {
        n = n+1;
    }
    
    public static void sumarRef(Integer n) {
        n = n+1;
        System.out.println(n);
    }
}
