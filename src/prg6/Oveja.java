/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

/**
 *
 * @author Zhel-PC
 */
public class Oveja {
    String nombre;
    boolean isProtected;
    boolean isAlive; //
    
    public Oveja(String n) {
        nombre = n;
        isProtected = false;
        isAlive = true;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public boolean isIsAlive() {
        return isAlive;
    }
    
    public boolean kill() {
        if (!isProtected) {
            isAlive = false;
        } else {
            isProtected = false;
        }
        
        return !isAlive;
    }
    
    public void protect() {
        isProtected = true;
    }
    
    
     
}
