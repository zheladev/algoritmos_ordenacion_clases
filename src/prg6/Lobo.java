/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

/**
 *
 * @author Zhel-PC
 */
public class Lobo extends Canino {
    String manada;
    
    public Lobo(String nombre, String nombreManada) {
        super(nombre);
        manada = nombreManada;
    }
    
    @Override
    public void ladrar() {
        System.out.println("Auuu! Soy un lobo y me llamo "+nombre);
    }
    
    public void pegarZarpazo() {
        System.out.println("Te reviento de un zarpazo.");
    }

    public void matarOveja(Oveja o) {
        boolean didKill = o.kill();
        if (didKill) {
            System.out.println(nombre + " ha matado a la oveja "+o.getNombre()+".");
        } else {
            System.out.println(nombre + " no ha conseguido matar a " + o.getNombre() + ".");
        }
        
    }
    
    @Override
    public void interactuarConOveja(Oveja o) {
        boolean didKill = o.kill();
        if (didKill) {
            System.out.println(nombre + " ha matado a la oveja "+o.getNombre()+".");
        } else {
            System.out.println(nombre + " no ha conseguido matar a " + o.getNombre() + ".");
        }
        
        
    }
}
