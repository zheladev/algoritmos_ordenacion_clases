/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

import java.util.ArrayList;

/**
 *
 * @author Zhel-PC
 */
public class MainPolimorfismoAvanzado {
    public static void main(String[] args) {
        ArrayList<Canino> caninos = new ArrayList<>();
        
        Canino canino1 = new Perro("Rex");
        Canino canino2 = new Lobo("LoboCanino", "Manada2");
        Perro perro1 = new Perro("Max");
        Lobo lobo1 = new Lobo("LoboGuay", "Manada1");
        
        caninos.add(perro1);
        caninos.add(canino1);
        caninos.add(lobo1);
        caninos.add(canino2);
        caninos.add(new Perro("Perro5"));
        
        perro1.ladrar();
        lobo1.ladrar();
        
        double db = 10.5;
        //casting de double a int
        int n = (int)db;
        
        //casting de Canino a Perro
        System.out.println("Ladrar con casting");
        ((Perro)canino1).mearEnRellano();

        perro1.mearEnRellano();
        
        System.out.println("Recorrer el array caninos\n\n\n");
        for(int i = 0; i < caninos.size();i++) {
            Canino c = caninos.get(i);
            c.ladrar();
            if(c instanceof Perro) {
                //Siempre que entremos aqui, c va a ser un objeto de tipo Perro
                ((Perro) c).mearEnRellano();
            }
        }
    }
}
