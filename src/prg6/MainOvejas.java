/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

import java.util.ArrayList;

/**
 *
 * @author Zhel-PC
 */
public class MainOvejas {
    public static void main(String[] args) {
        ArrayList<Canino> caninos = new ArrayList<>();
    
        Canino canino1 = new Perro("Rex");
        Canino canino2 = new Lobo("LoboCanino", "Manada2");
        Perro perro1 = new Perro("Max");
        Lobo lobo1 = new Lobo("LoboGuay", "Manada1");

        caninos.add(perro1);
        caninos.add(canino1);
        caninos.add(lobo1);
        caninos.add(new Perro("Perro4"));
        caninos.add(new Perro("Perro6"));
        caninos.add(new Perro("Perro5"));
        
        Oveja oveja1 = new Oveja("Shawn");

        
        while(oveja1.isIsAlive()) {
            int rand = (int)(Math.random()*caninos.size());
            Canino c = caninos.get(rand);
            if (c instanceof Perro) {
                ((Perro) c).protegerOveja(oveja1);
            } else if (c instanceof Lobo) {
                ((Lobo) c).matarOveja(oveja1);
            }
        }
    }
}
