/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

/**
 *
 * @author Zhel-PC
 */
public class Perro extends Canino {
    
    public Perro(String nombre) {
        super(nombre);
    }

    @Override
    public void ladrar() {
        System.out.println("Guau! Soy un perro y me llamo "+nombre);
    }
    
    public void mearEnRellano() {
        System.out.println("Me he meado en el rellano.");
    }

    public void protegerOveja(Oveja o) {
        o.protect();
        System.out.println("El perro "+nombre+" está protegiendo a la oveja "+o.getNombre()+".");
    }
    
    @Override
    public void interactuarConOveja(Oveja o) {
        o.protect();
        System.out.println("El perro "+nombre+" está protegiendo a la oveja "+o.getNombre()+".");
    }
}
