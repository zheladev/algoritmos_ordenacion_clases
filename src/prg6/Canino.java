/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg6;

/**
 *
 * @author Zhel-PC
 */
public abstract class Canino {
    final int NUM_CEREBRO = 1;
    int patas;
    String nombre;
    
    public Canino(String nombre) {
        patas = 4;
        this.nombre = nombre;
    }
  
    public String getNombre() {
        return nombre;
    }
    
    public abstract void ladrar();
    
    public abstract void interactuarConOveja(Oveja o);
}
