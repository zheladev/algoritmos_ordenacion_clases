/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author Zhel-PC
 */
public class Producto {
    static int numProductosCreados = 0;
    int idProd;
    String desc;
    int cant;
    float precio;
    
    public Producto(int cantidad, float precio, String descripcion, int idCliente) {
        idProd = ++numProductosCreados*1000 + idCliente;
        desc = descripcion;
        cantidad = cant;
        this.precio = precio;
    }

    public int getIdProd() {
        return idProd;
    }

    public String getDesc() {
        return desc;
    }

    public int getCant() {
        return cant;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public String toString() {
        return "ID: " + String.format("%07d", idProd) + "\tPrecio: " + String.format("%.2f", precio) + "€\tCantidad: " + cant + "\tDescripción: " + desc ;
    }
    
    
}
