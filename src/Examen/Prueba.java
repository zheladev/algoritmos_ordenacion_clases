/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

/**
 *
 * @author Zhel-PC
 */
public class Prueba {
    static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        HashSet<Cliente> clientes = new HashSet<>();
        int opt = 1, opt2 = 1; 
        
        //bucle principal
        while (opt > 0) {
            Cliente cliente = introducirCliente();
            
            //bucle de carrito de compra del cliente
            opt2 = 1;
            while (opt2 > 0) {
                Producto producto = introducirProducto(cliente.getNumCliente());
                
                cliente.addProducto(producto);
                System.out.println("Quiere seguir introduciendo productos al carrito del cliente? 1 = si, 0 = no");
                opt2 = Integer.parseInt(scan.nextLine().trim()); //trim para eliminar el newline
            }
            
            clientes.add(cliente);
            //ver si se quieren seguir introduciendo clientes
            System.out.println("Quiere seguir introduciendo clientes? 1 = sí, 0 = no");
            opt = Integer.parseInt(scan.nextLine().trim()); //trim para eliminar el newline
        }
        
        System.out.println(clientes);
    }

    private static Cliente introducirCliente() {
        String clientName, clientDir;
        boolean isClientVip;
        
        System.out.println("Nombre cliente: ");
        clientName = scan.nextLine();

        System.out.println("Direccion cliente: ");
        clientDir = scan.nextLine();
        System.out.println("Es el cliente VIP? (escriba 'si' si lo es.): ");
        isClientVip = scan.nextLine().equalsIgnoreCase("si");

        Cliente c = new Cliente(clientName, clientDir, isClientVip);
        
        return c;
    }

    private static Producto introducirProducto(int idCliente) {
        String prodDesc;
        int prodCant;
        float prodPrecio;
        
        System.out.println("Descripcion producto: ");
        prodDesc = scan.nextLine();
        
        System.out.println("Precio producto (formato X.XX): ");
        prodPrecio = Float.parseFloat(scan.nextLine().trim()); //trim para eliminar el newline
        
        System.out.println("Cantidad: ");
        prodCant = Integer.parseInt(scan.nextLine().trim()); //trim para eliminar el newline
        
        Producto p = new Producto(prodCant, prodPrecio, prodDesc, idCliente);
        return p;
    }
    
    public static void tarea1() {
        String frase1, frase2;
        HashMap<String, Integer> frecuencia = new HashMap<>();
        HashSet<String> palabras = new HashSet();
        
        System.out.println("Frase uno: ");
        frase1 = scan.nextLine();
        System.out.println("Frase dos: ");
        frase2 = scan.nextLine();
        
        String[] fraseUnoPalabras = frase1.split(" ");
        String[] fraseDosPalabras = frase2.split(" "); //también podéis usar un StringTokenizer
        //se guarda solo una copia de cada palabra diferente por ser un hashset
        palabras.addAll(Arrays.asList(fraseUnoPalabras));
        
        //recorre el hashset y va contando la frecuencia de las palabras en el hashmap
        for(String palabra : fraseDosPalabras) {
            if(palabras.contains(palabra)) {
                //la palabra de la frase 2 está en la lista de palabras de la frase 1, hay que contabilizarla
                if(frecuencia.containsKey(palabra)) {
                    frecuencia.put(palabra, frecuencia.get(palabra)+ 1);
                } else {
                    frecuencia.put(palabra, 1);
                }
            }
        }
        
    }
}
