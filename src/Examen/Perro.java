/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

/**
 *
 * @author Zhel-PC
 */
public class Perro {
    String nombre;
    int id;
    int patas;
    
    public Perro(String nombre) {
        this.nombre = nombre;
    }
    
    public String getNombre() {
        return nombre;
    }
    
    public void ladrar() {
        System.out.println("Soy " + nombre + ". Guau!");
    }
    
    public String toString() {
        return "Soy un perro, y me llamo " + nombre;
    }
}
 
