/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Examen;

import java.util.ArrayList;

/**
 *
 * @author Zhel-PC
 */
public class Cliente {
    static int numClientesCreados = 0;
    int numCliente;
    String nombre;
    String direccion;
    boolean isVip;
    ArrayList<Producto> carrito;
    
    public Cliente(String nom, String dir, boolean vip) {
        numCliente = ++numClientesCreados;
        nombre = nom;
        direccion = dir;
        isVip = vip;
        carrito = new ArrayList<>();
    }

    public void addProducto(Producto p) {
        carrito.add(p);
    }
    
    public int getNumCliente() {
        return numCliente;
    }
    
    public String getNumClienteString() {
        return String.format("%03d", numCliente);
    }
    
    public String toString() {
        String userInfo = "ID CLIENTE: " + String.format("%03d", numCliente) + "\n"
                + "NOMBRE: " + nombre + "\n"
                + "DIRECCION: " + direccion + "\n"
                + (!isVip?"NO ":"") + "ES VIP\n"
                + "CARRITO\n";
        String productInfo = "";
        
        for(Producto p : carrito) {
            productInfo += "\t" + p.toString();
        }
        return userInfo + productInfo;
    }
    
    
    
}
