/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claseFicheros;

import java.io.*;

/**
 *
 * @author Zhel-PC
 */
public class LecturaStream {
    public static void main(String[] args) {
        File f = new File("ficheros/holamundo.txt");
        String datos = "";
        try {
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            
            //escribir el contenido del archivo en datos
            while(true) {
                datos += (char)dis.readByte();
            }
        } catch(EOFException eof) {
            System.out.println(datos);
            System.out.println("Se ha acabado de leer el documento.");
        } catch(IOException ioe) {
            
        }
     
        
    }
}
