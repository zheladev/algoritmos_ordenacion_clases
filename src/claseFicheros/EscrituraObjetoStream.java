/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claseFicheros;
import java.io.*;

/**
 *
 * @author Zhel-PC
 */
public class EscrituraObjetoStream {
    public static void main(String[] args){
        Persona persona = new Persona("Juan");
        
        //guardar datos en personas.bin
        try {
            File f = new File("ficheros/personas.bin");
            FileOutputStream fos = new FileOutputStream(f);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            
            oos.writeObject(persona);
            oos.close();
        } catch (IOException ex) {
            
        }
        
        try {
            File f = new File("ficheros/personas.bin");
            FileInputStream fos = new FileInputStream(f);
            ObjectInputStream ois = new ObjectInputStream(fos);

            Persona p = (Persona)ois.readObject();
            System.out.println(p.nombre);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        
    }
}
