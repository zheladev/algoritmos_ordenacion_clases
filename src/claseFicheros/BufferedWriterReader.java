 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claseFicheros;

import java.io.*;

/**
 *
 * @author Zhel-PC
 */
public class BufferedWriterReader {
    public static void main(String[] args) {
        File f = new File("ficheros/nombres.txt");

        String[] nombres = {"SERgio", "emiLiano", "JUaNmNnN", "mAria", "EustAquio", "pAuLa"};
        try {
            FileWriter fw = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(fw);
            
            for(String nombre : nombres) {
                bw.write(nombre + "\n");
            }
            bw.close();
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        
        try {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            
            String dato = ""; 
            
            dato = br.readLine();
            
            while(dato != null) {
                System.out.println(dato);
                dato = br.readLine();
            }
            
        } catch(IOException e) {
            System.out.println(e.getMessage());
        }
        
        
        
    }
}
