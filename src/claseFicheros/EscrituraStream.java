/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package claseFicheros;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author Zhel-PC
 */
public class EscrituraStream {
        public static void main(String[] args) {
        File f = new File("ficheros/escrituraStream.txt");
        String datos = "";
        
        try {
           FileOutputStream fos = new FileOutputStream(f);
           DataOutputStream dos = new DataOutputStream(fos);
           
           dos.writeBoolean(true);
           dos.writeBoolean(true);
           dos.writeBoolean(false);
           dos.writeBoolean(true);
           dos.close();
        } catch (IOException e) {
            
        }
        
        try {
            FileInputStream fis = new FileInputStream(f);
            DataInputStream dis = new DataInputStream(fis);
            
            boolean b1 = dis.readBoolean();
            boolean b2 = dis.readBoolean();
            boolean b3 = dis.readBoolean();
            boolean b4 = dis.readBoolean();
            
            if (b1 && b2 && !b3 && b4) {
                System.out.println("Hemos leido el archivo correctamente.");
            }
        } catch (IOException e) {
            
        }
        
    }
}
