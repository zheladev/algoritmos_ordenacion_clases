package practicasPruebas;
import java.io.*;

public class EscrituraPorPantalla {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		PrintWriter pantalla = new PrintWriter(System.out);
		char[] array = {'M','O','R','E','N','O'};
		
		String  str = new String ("Juan Carlos");
		
		pantalla.write(str);
		pantalla.print(" ");
		pantalla.write(array, 0, 6);
		pantalla.println(" ");
		pantalla.flush();	

	}

}
