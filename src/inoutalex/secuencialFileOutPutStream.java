package practicasPruebas;

import java.io.*;

public class secuencialFileOutPutStream {

	public static void main(String[] args) {

		String[] amigos = { "Andres Rosique", "Pedro Ruiz", "Isaac Sanchez", "Juan Serrano" };

		long[] telefonos = { 653364787, 653364786, 653364785, 653364784 };

		// escritura del fichero

		try {

			FileOutputStream fs = new FileOutputStream("amigos.txt");
			DataOutputStream d = new DataOutputStream(fs);

			for (int i = 0; i < 4; i++) {
				d.writeUTF(amigos[i]);
				d.writeLong(telefonos[i]);
			}

			if (d != null) {
				d.close();
				fs.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		// lectura del fichero

		try {

			File f = null;
			FileInputStream fe = null;
			DataInputStream d = null;
			try {

				f = new File("amigos.txt");

				if (f.exists()) {
					fe = new FileInputStream(f);
					d = new DataInputStream(fe);
					String s;
					Long l;

					while (true) {
						s = d.readUTF();
						System.out.println(s + " -> ");
						l = d.readLong();
						System.out.println(l);
					}
				}
			} catch (EOFException eof) {
				System.out.println("------------------------------------");

			} catch (FileNotFoundException fnf) {

				System.err.println("Fichero no encontrado " + fnf);

			} catch (IOException e) {
				System.out.println("Se a producido ua IOException");
				e.printStackTrace();

			} catch (Throwable e) {
				System.err.println("Error de programa: " + e);
				e.printStackTrace();

			} finally {
				if (d != null) {
					d.close();
					fe.close();
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}