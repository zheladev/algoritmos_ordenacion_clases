package practicasPruebas;
import java.io.*;

public class EjemploFlujos {

	public static void main (String[] args) {
		
		String s = new String("Primer texto");
		
		s = s + ", Segundo texto";
		s = s + ", Tercer texto";
		
		char[] arr = new char[s.length()];
		
		int car = 0;
		
		StringReader flujoInput = new StringReader(s);	
		
		CharArrayWriter flujoOutput = new CharArrayWriter();
		
		try {
			while ((car = flujoInput.read()) != -1) {
				flujoOutput.write(car);
			}
			arr = flujoOutput.toCharArray();
			System.out.println(arr);
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			flujoInput.close();
			flujoOutput.close();
		}
	}
}
