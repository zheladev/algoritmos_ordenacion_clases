package practicasPruebas;
import java.io.*;

public class EjemploLecturaPorConsola {
	
	public static String leercadena(){
		
		String cad = "";
		
		BufferedReader br;
		
		br = new BufferedReader (new InputStreamReader(System.in));
		
		try {
			
			cad = br.readLine();
			
		}catch (IOException e) {
			e.printStackTrace();
			
		}
		
		return cad;
	}
	
	public static void main(String args[]) throws IOException {
		String cad;
		System.out.print("Escribe para para que el programa pare");
		do {
			cad = leercadena();
			System.out.print(cad);
		}while(!cad.equals("para"));
		
	}

}
