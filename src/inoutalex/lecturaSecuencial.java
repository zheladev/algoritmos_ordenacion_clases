package practicasPruebas;

import java.io.*;

public class lecturaSecuencial {

	public static void main(String[] args) {

		FileOutputStream f = null;
		
		String s = "En un lugsr de la mancha...";
		
		char c=0;
		
		try {
			
			f = new FileOutputStream("datos.rxt");
			
			for (int i=0; i<s.length();i++) {
				c=s.charAt(i);
				f.write((byte)c);
			}
		}catch(IOException e) {
			e.printStackTrace();
		}finally {
			try {
				f.close();
			}catch(IOException e) {
				e.printStackTrace();
			}
		}
	}

}
