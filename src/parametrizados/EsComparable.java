/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

/**
 *
 * @author Zhel-PC
 */
public interface EsComparable<T> {
    public boolean mayorQue(T dato);
    public boolean menorQue(T dato);
}
