/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

/**
 *
 * @author Zhel-PC
 */
public class Ordenador implements EsComparable<Ordenador> {
    int ghz;
    String nombre;
    
    public Ordenador(int ghz, String n) {
        this.ghz = ghz;
        nombre = n;
    }

    @Override
    public boolean mayorQue(Ordenador dato) {
        return ghz > dato.ghz;
    }

    @Override
    public boolean menorQue(Ordenador dato) {
        return ghz < dato.ghz;
    }
    
}
