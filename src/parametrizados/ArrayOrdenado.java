/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

import java.util.Vector;

/**
 *
 * @author Zhel-PC
 * @param <T>
 */
public class ArrayOrdenado<T extends EsComparable<T>> {
    int aSize;
    Vector<T> contenedor;
    
    public ArrayOrdenado() {
        contenedor = new Vector<>();
    }
    
    public void add(T objeto) {
        int indiceAInsertar = 0;
        boolean encontradoMayor = false;
        
        for(int i = 0; i < contenedor.size() && !encontradoMayor; i++) {
            if(objeto.menorQue(contenedor.get(i))) {
                encontradoMayor = true;
                contenedor.add(i, objeto);
            }
        }
        
        if (!encontradoMayor) {
            contenedor.add(objeto);
        }
    }
    
    public T get(int index) {
        return contenedor.get(index);
    }
    
    public boolean esMayorQue(T obj1, T obj2) {
        return obj1.mayorQue(obj2);
    }
    
    @Override
    public String toString() {
        String arrStr = "[ ";
        for (int i = 0; i < contenedor.size(); i++) {
            arrStr += "\""+contenedor.get(i).toString()+"\", ";
        }
        arrStr += "]";
        return arrStr;
    }
}
