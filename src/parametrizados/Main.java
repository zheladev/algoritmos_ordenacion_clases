/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

import java.util.ArrayList;

/**
 *
 * @author Zhel-PC
 */
public class Main {
    public static void main(String[] args) {
        ArrayOrdenado<Raton> ratones = new ArrayOrdenado<>();

        Raton raton1 = new Raton(100, "Logitech");
        Raton raton2 = new Raton(50, "Razer");
        Raton raton3 = new Raton(75, "Ratonde75");

        
        ratones.add(raton3);
        ratones.add(raton2);
        ratones.add(raton1);
        
        System.out.println(ratones);
    }
}
