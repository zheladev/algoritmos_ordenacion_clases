/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

/**
 *
 * @author Zhel-PC
 */
public class Raton implements EsComparable<Raton> {
    int velocidad;
    String nombre;
    
    public Raton(int velocidad, String nombre) {
        this.velocidad = velocidad;
        this.nombre = nombre;
    }
    
    public int getVelocidad() {
        return velocidad;
    }
    
    public String getNombre() {
        return nombre;
    }

    @Override
    public boolean mayorQue(Raton dato) {
        return velocidad > dato.getVelocidad();
    }

    @Override
    public boolean menorQue(Raton dato) {
        return velocidad < dato.getVelocidad();
    }
    
    @Override
    public String toString() {
        return nombre + " velocidad: " + velocidad;
    }
}
