/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parametrizados;

/**
 *
 * @author Zhel-PC
 */
public class RatonSinInterfaz {
        int velocidad;
    String nombre;
    
    public RatonSinInterfaz(int velocidad, String nombre) {
        this.velocidad = velocidad;
        this.nombre = nombre;
    }
    
    public int getVelocidad() {
        return velocidad;
    }
    
    public String getNombre() {
        return nombre;
    }
   
    public boolean mayorQue (Raton dato) {
        return velocidad > dato.getVelocidad();
    }
}
