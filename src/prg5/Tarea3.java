/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg5;

import java.util.Vector;

/**
 *
 * @author Zhel-PC
 */
public class Tarea3 {

    public static void main(String[] args) {
        //TODO script de creación de matriz sobre la que aplicar normalizarMatriz
    }

    public static void normalizarMatriz(double[][] matriz) {
        double min = findMinValue(matriz);
        double max = findMaxValue(matriz);

        //evitamos divisiones entre 0
        if (max > min) {
            for (int i = 0; i < matriz.length; i++) {
                for (int j = 0; j < matriz[i].length; j++) {
                    matriz[i][j] = normalizar(matriz[i][j], min, max);
                }
            }
        }
    }

    public static double normalizar(double n, double min, double max) {
        return ((n - min) / (max - min)) - min;
    }

    public static double findMinValue(double[][] m) {
        double min = m[0][0];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (m[i][j] < min) {
                    min = m[i][j];
                }
            }
        }
        return min;
    }

    public static double findMaxValue(double[][] m) {
        double max = m[0][0];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (m[i][j] > max) {
                    max = m[i][j];
                }
            }
        }
        return max;
    }

    public static boolean hasRepeatingNumbersV(double[][] m) {
        boolean repeated = false;
        Vector nums = new Vector();

        for (int i = 0; i < m.length && !repeated; i++) {
            for (int j = 0; i < m[i].length && !repeated; j++) {
                if (nums.contains(m[i][j])) {
                    repeated = true;
                }
                nums.add(m[i][j]);
            }
        }
        return repeated;
    }
}
