/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg5;

/**
 *
 * @author Zhel-PC
 */
public class Sorting {

    /**
     * Desordena un array.
     *
     * @param arr int[] array a desordenar
     */
    static void shuffleArray(int[] arr) {
        for (int i = arr.length - 1; i > 0; i--) {
            int index = (int) (Math.random() * arr.length);
            int a = arr[index];
            arr[index] = arr[i];
            arr[i] = a;
        }
    }

    /**
     * Comprueba si un array está ordenado.
     *
     * @param arr int[] array a comprobar
     */
    static boolean isSorted(int[] arr) {
        //Comprobamos si el array contiene uno o más valores y guardamos su
        //primer valor
        boolean unsorted = false;
        int n = arr.length > 0 ? arr[0] : 0;

        for (int i = 1; i < arr.length && !unsorted; i++) {
            if (arr[i] < n) {
                unsorted = true;
            }
            n = arr[i];
        }

        return unsorted;
    }

    /**
     * Ordena un array implementando el algoritmo insertion sort
     *
     *
     * @param arr array que ordenar
     * @return boolean que representa si se ha ordenado correctamente
     */
    static void insertionSort(int[] arr) {
        int aux;
        int j;
        //recorremos array desde el segundo indice hasta el último
        for (int i = 1; i < arr.length; i++) {
            //guardamos valor de arr[i] 
            aux = arr[i];
            //usamos j para recorrer el array (como si lo declarásemos en el for)
            j = i - 1;
            //recorre array desde i-1, intercambiando arr[j] con el de i-1 hasta que encontremos
            //un valor más pequeño que aux
            while (j >= 0 && arr[j] > aux) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = aux;
        }
    }

    /**
     * Ordena un array implementando el algoritmo selection sort
     *
     * @param arr int[] array a ordenar
     */
    static void selectionSort(int[] arr) {
        int minIdx; //variable donde almacenamos el indice del valor más pequeño de cada iteración
        int aux; //variable auxiliar para intercambiar dos valores dentro del array

        //recorre desde el primer hasta el penúltimo índice del array
        for (int i = 0; i < arr.length - 1; i++) {
            minIdx = i; //guardamos i como el indice del valor mínimo (hasta que encontremos otro más pequeño si existe)
            //iteramos desde i+1 hasta el final del array
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIdx]) {
                    //si encontramos un valor más pequeño que el guardado en arr[minIdx] guardamos su indice
                    minIdx = j;
                }
            }

            //intercambiamos los valores de i y minIdx para tener el valor más pequeño ordenado.
            if (minIdx != i) {
                aux = arr[minIdx];
                arr[minIdx] = arr[i];
                arr[i] = aux;
            }
        }
    }

    /**
     * Ordena un array implementando el algoritmo bubble sort
     *
     * @param arr int[] array a ordenar
     */
    static void bubbleSort(int[] arr) {
        int aux;

        //iteramos del primer al penúltimo indice
        for (int i = 0; i < arr.length - 1; i++) {
            //por cada índice iteramos desde el último elemento hasta dicho índice
            for (int j = arr.length - 1; j > i; j--) {
                //si el valor del índice anterior a j es menor que el de j, los intercambiamos
                if (arr[j - 1] > arr[j]) {
                    aux = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = aux;
                }
            }
        }
    }

    static void cocktailSort(int[] arr) {
        int aux, left, right;
        left = 0; //representa un puntero que recorre de izq. a derecha el array
        right = arr.length - 1; //representa un puntero que recorre de derecha a izq. el array

        //mientras los punteros no se hayan encontrado
        while (left < right) {
            //recorremos de un puntero a otro (izq. a der.)
            for (int i = left; i < right; i++) {
                //si el elemento i+1 es mayor que el de i
                if (arr[i] > arr[i + 1]) {
                    //intercambiamos valores
                    aux = arr[i + 1];
                    arr[i + 1] = arr[i];
                    arr[i] = aux;
                }
            }
            //movemos el puntero right una posición a la izquierda
            right--;

            //si aún no se han tocado los punteros
            if (left != right) {
                //recorremos desde el otro puntero al primero (der. a izq.)
                for (int i = right; i > left; i--) {
                    //si el elemento i-1 es mayor que el de i
                    if (arr[i - 1] > arr[i]) {
                        //intercambiamos valores
                        aux = arr[i - 1];
                        arr[i - 1] = arr[i];
                        arr[i] = aux;
                    }
                }
                //movemos el puntero left una posición hacia la derecha
                left++;
            }

        }
    }

    private static void _mergesort(int[] arr, int length) {
        int mid;
        //si hay más de dos elementos en el array
        if (length >= 2) {
            //calculamos la mitad del array
            mid = length / 2;

            //creamos dos subarrays
            int[] leftArr = new int[mid];
            int[] rightArr = new int[length - mid];

            //copiamos la mitad izquierda de arr en leftArr
            for (int i = 0; i < mid; i++) {
                leftArr[i] = arr[i];
            }

            //copiamos la mitad derecha de arr en rightArr
            for (int i = mid; i < length; i++) {
                rightArr[i - mid] = arr[i];
            }
            _mergesort(leftArr, mid);
            _mergesort(rightArr, length - mid);

            _merge(arr, leftArr, rightArr, mid, length - mid);
        }
    }

    /** Black magic
     * 
     * @param arr array completo a ordenar
     * @param l subarray izquierdo de arr
     * @param r subarray derecho de arr
     * @param left 
     * @param right 
     */
    private static void _merge(int[] arr, int[] l, int[] r, int left, int right) {
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i] <= r[j]) {
                arr[k++] = l[i++];
            } else {
                arr[k++] = r[j++];
            }
        }
        while (i < left) {
            arr[k++] = l[i++];
        }
        while (j < right) {
            arr[k++] = r[j++];
        }
    }

    static void mergeSort(int[] arr) {
        _mergesort(arr, arr.length);
    }

    private static void _quicksort(int[] arr, int left, int right) {
        int pivot = arr[left];
        int i = left; //punter que recorrerá desde la izq hasta el pivote
        int j = right; //pivote que recorrerá desde la derecha hasta el primer elemento menor que pivote (o el pivote)
        int aux;

        //si los punteros no están cruzados
        if (left < right) {
            //mantiene el puntero i a la izquierda del pivote y el puntero j a la derecha
            while (i < j) {
                //buscamos el indice del primer elemento en la parte izquierda mayor que el pivote
                while (arr[i] <= pivot && i < j) {
                    i++;
                }
                //buscamos el indice del primer elemento empezando por la derecha menor que el pivote
                while (arr[j] >= pivot && j >= i) {
                    j--;
                }
                //si no se han cruzado los punteros, hay un elemento mayor que pivot en la izq. y uno menor en la der.
                if (i < j) {
                    aux = arr[i];
                    arr[i] = arr[j];
                    arr[j] = aux;
                }
            }
            arr[left] = arr[j];
            arr[j] = pivot;
            _quicksort(arr, left, right - 1);
            _quicksort(arr, left + 1, right);
        }
    }

    static void quickSort(int[] arr) {
        _quicksort(arr, 0, arr.length - 1);
    }
}
