/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg5;

import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author Zhel-PC
 */
public class Colecciones {
    public static void main(String[] args) {
        /**
         * Inicialización e instanciado
         */
        //array
        int[] numbersArray = new int[10];
        //vector
        Vector numbersVector = new Vector(10); //vector de tamaño 10 (dobla su tamaño si sobrepasamos el límite)
        //Vector numbersVector = new Vector(10, 5); //vector de tamaño 10 (añade 5 a su tamaño si sobrepasamos el límite)
        //arraylist
        ArrayList<Integer> numbersArrayList = new ArrayList<Integer>(); //no se pueden usar tipos primitivos, no hay que especificar tamaño
        
        
        /**
         * Añadir elementos
         */
        //array
        numbersArray[0] = 1; //guardamos 1 en el primer índice
        //vector
        numbersVector.add(0, 1); //guardamos 1 en el índice 0
        numbersVector.add(2); //guardamos 2 al final del vector
        //arraylist
        numbersArrayList.add(0, 1); //guardamos 1 en el índice 0
        numbersArrayList.add(1); //guardamos 1 en el último índice
        
        
        /**
         * Obtener tamaño
         */
        System.out.println(numbersArray.length);
        
        System.out.println(numbersVector.capacity()); //tamaño del vector
        System.out.println(numbersVector.size()); // numero de elementos guardados en el vector
        
        System.out.println(numbersArrayList.size()); //numero de elementos guardados en el vector
        
        
        /**
         * Obtener elemento en base a índice
         */
        int index = 3;
        //array
        System.out.println(numbersArray[index]);
        //vector
        System.out.println(numbersVector.elementAt(index));
        //arraylist
        System.out.println(numbersArrayList.get(index));
        
        /**
         * Modificar elemento ya guardado
         */
        int indexToModify = 1;
        //array
        numbersArray[indexToModify] = 100;
        //vector
        numbersVector.set(indexToModify, 100);
        //arraylist
        numbersArrayList.set(indexToModify, 100);
        
        
        /**
         * Eliminar elemento
         */
        int indexToDelete = 1; //queremos borrar el número 1
        //array
        numbersArray[indexToDelete] = 0;
        //vector
        numbersVector.remove(indexToDelete);
        //arraylist
        numbersArrayList.remove(indexToDelete);
        
        
        /**
         * Buscar elemento
         */
        int numberToFind = 100;
        //array
        //al acabar el bucle for idx contendrá el índice en el que se encuentra numberToFind, o -1 si no se encuentra en el array
        int idx = -1;
        for(int i = 0; i < numbersArray.length && idx == -1; i++) {
            if (numberToFind == numbersArray[i]) {
                idx = i;
            }
        }
        //vector
        idx = numbersVector.indexOf(numberToFind); // indice si lo encuentra, si no -1
        //arraylist
        idx = numbersArrayList.indexOf(numberToFind); //indice si lo encuentra, si no -1
        
        
    }
}
