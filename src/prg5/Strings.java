package prg5;


import java.util.Scanner;

public class Strings {

	public static void main(String[] args) {
            capitalizarPalabras();
		Scanner tec = new Scanner(System.in);
	        System.out.println("Introduce una cadena: ");

	        String cd = tec.nextLine();	  
                
                String palabraMasLarga = "";
	        
                //separamos las palabras y las metemos en un array de Strings
                String[] palabras = cd.split(" "); //el argumento de split es el delimitador que separa los diferentes elementos del array

                //recorremos las palabras
                for (String palabra : palabras) {
                    //si la palabra en la que nos encontramos es más larga que la que tenemos guardada como más larga
                    if (palabra.length() > palabraMasLarga.length()) {
                        //guardamos dicha palabra como la palabra más larga
                        palabraMasLarga = palabra;
                    }
                }
                
                System.out.println("Palabra más larga: " + palabraMasLarga);
                System.out.println("Tamaño de la palabra más larga: " + palabraMasLarga.length()); //.length() nos devuelve el tamaño de la palabra
	}
        
        public static void capitalizarPalabras() {
            Scanner tec = new Scanner(System.in);
            String cadenaCapitalizada = "";
            
            System.out.println("Introduce cadena: ");
            String cadenaACapitalizar = tec.nextLine();
            
            //separamos cadena por palabras
            String[] palabras = cadenaACapitalizar.split(" ");
            
            //recorremos las palabras
            for(int i = 0; i < palabras.length; i++) {
                String palabra = palabras[i];
                
                //comprobamos si la primera letra de la palabra es una minúscula
                if(palabra.charAt(0) >= 'a' && palabra.charAt(0) <= 'z') {
                    String primeraLetraMayuscula = palabra.substring(0, 1).toUpperCase(); //cogemos la primera letra y la hacemos mayúscula
                    String restoPalabra = "";
                    //comprobamos si la palabra está formada por más de una letra para no intentar acceder a un índice inexistente
                    if (palabra.length() > 1) {
                        //guardamos el resto de la palabra (menos la primera letra, que la hemos guardado en mayúscula en la variable primeraLetraMayuscula)
                        restoPalabra = palabra.substring(1);
                    }
                    palabra =  primeraLetraMayuscula + restoPalabra; //volvemos a juntar la primera letra capitalizada + el resto de la palabra
                }
                
                cadenaCapitalizada += palabra + " "; //añadimos al String cadenaCapitalizada la palabra con su primera letra en mayúsucla
            }
            System.out.println(cadenaCapitalizada);
        }
}