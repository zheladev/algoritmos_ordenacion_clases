/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg5;

import java.util.Scanner;

/**
 *
 * @author Zhel-PC
 */
public class EjercicioPractica {
    
    final static boolean CREAR_ORDENADAMENTE = false;
    final static int CAPACITY = 50;
    final static String MENU_STRING = "Para insertar un elemento, pulse 1"
            + "\nPara eliminar un elemento, pulse 2"
            + "\nPara buscar un elemento, pulse 3"
            + "\nPara mostrar el vector, pulse 4"
            + "\nPara crear un vector, pulse 5"
            + "\nPara salir, pulse 0"
            + "\nOpción: ";
    final static String INSERT_STRING = "Indique el número a insertar: ";
    final static String DELETE_STRING = "Indique el número a eliminar: ";
    final static String SEARCH_STRING = "Indique el número a buscar: ";
    final static String SUCCESS_STRING = "Acción realizada con éxito.";
    final static String ERROR_STRING = "Fallo al realizar la acción.";
    final static String GOODBYE_STRING = "Hasta la vista, baby.";
    final static String CREATION_STRING = "Dime upper bound del vector (> " + CAPACITY + "): ";

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int[] arr = new int[CAPACITY];
        int curr_size = 0;
        int opt = -1, opt2, pos;

        
        
        while (opt != 0) {
            System.out.println("TAMANO: " + curr_size);
            System.out.println(MENU_STRING);
            opt = scan.nextInt();

            switch (opt) {
                case 1: //insertar
                    System.out.println(INSERT_STRING);
                    opt2 = scan.nextInt();
                    if (insertar(arr, opt2, curr_size)) {
                        System.out.println(SUCCESS_STRING);
                        curr_size++;
                    } else {
                        System.out.println(ERROR_STRING);
                    }
                    break;
                case 2: //eliminar
                    System.out.println(DELETE_STRING);
                    opt2 = scan.nextInt();
                    if (eliminar(arr, opt2, curr_size)) {
                        System.out.println(SUCCESS_STRING);
                        curr_size--;
                    } else {
                        System.out.println(ERROR_STRING);
                        System.out.println("Está lleno su vector?");
                    }
                    break;
                case 3: //buscar
                    System.out.println(SEARCH_STRING);
                    opt2 = scan.nextInt();
                    pos = buscar(arr, opt2, curr_size, true);
                    if (pos > -1) {
                        System.out.println("Posición en la que está/estaría "
                                + opt2 + ": " + pos);
                    } else {
                        System.out.println(ERROR_STRING);
                    }
                    break;
                case 4: //mostrar
                    printArray(arr, curr_size);
                    break;
                case 5: // ejercicio 2
                    System.out.println(CREATION_STRING);
                    do {
                        opt2 = scan.nextInt();
                    } while(opt2 < CAPACITY+1);
                    arr = crearVector(opt2);
                    curr_size = 50;
                    break;
                case 0: //terminar programa
                    System.out.println(GOODBYE_STRING);
                    break;
                default:
                    System.out.println(opt + " no es una opción válida.");
                    break;
            }
        }

    }

    //ejercicio 5
    static int[] crearVector(int n) {
        int[] v = new int[CAPACITY];
        int size = 0, rnd;
        //creamos valores para el array hasta llegar a la capacidad del array
        while(size < CAPACITY) {
            //creamos numero aleatorio
            rnd = (int)(Math.random()*(n-1));
            //si no existe en el array
            if (buscar(v, rnd, size, true) == -1) {
                //lo insertamos
                if (CREAR_ORDENADAMENTE) {
                    insertar(v, rnd, size);
                } else {
                    v[size] = rnd;
                }
                //incrementamos el tamaño del array
                size++;
            }
        }
        
        return v;
    }
    
    static boolean insertar(int[] arr, int n, int size) {
        int pos = -2;

        // comprobamos que no esté lleno el array
        if (size < CAPACITY) {
            //buscamos posicion en la que insertar n
            pos = buscar(arr, n, size, false);
            //si no se ha encontrado, ni valores menores que n, se insertará al final del array
            if (pos == -1) {
                pos = size;
            }
            //desplazamos elementos hacia la derecha para hacer espacio
            for (int i = size; i > pos; i--) {
                arr[i] = arr[i - 1];
            }
            //insertamos n
            arr[pos] = n;
        }
        // si pos < -1, no se ha podido insertar.
        return pos > -1;
    }

    static boolean eliminar(int[] arr, int n, int size) {
        //buscamos el elemento
        int pos = buscar(arr, n, size, true);
        //si lo encontramos
        if (pos > -1) {
            //desplazamos todos los elementos a su derecha una posición a la izquierda
            for (int i = pos + 1; i < size; i++) {
                arr[i - 1] = arr[i];
            }
            arr[size-1] = 0; //borramos el último elemento
        }
        //si pos es menor que uno, significa que no se ha encontrado el elemento en el array, luego no se ha podido borrar
        return pos > -1;
    }
    
    
    static int buscar(int[] arr, int n, int size, boolean isEqual) {
        boolean encontrado = false;
        int pos = -1;
        for (int i = 0; i < size && !encontrado; i++) {
            if (n <= arr[i]) {
                encontrado = true;
                pos = i;
            }
        }
        if (pos != -1 && isEqual && arr[pos] != n) {
            pos = -1;
        }
        return pos;
    }

    static void printArray(int[] arr, int size) {
        System.out.print("[ ");
        for (int i = 0; i < size; i++) {
            System.out.print("" + arr[i] + ", ");
        }
        System.out.println(" ]");

    }

}
