/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prg5;

/**
 *
 * @author Zhel-PC
 */
public class PRG5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        
        System.out.println("------InsertionSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.insertionSort(arr);
        printArray(arr);
        
        System.out.println("\n\n------SelectionSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.selectionSort(arr);
        printArray(arr);
        
        System.out.println("\n\n------BubbleSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.bubbleSort(arr);
        printArray(arr);
        
        System.out.println("\n\n------CocktailSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.cocktailSort(arr);
        printArray(arr);
        
        System.out.println("\n\n------QuickSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.quickSort(arr);
        printArray(arr);
        
        System.out.println("\n\n------MergeSort------");
        System.out.println("Array desordenado: ");
        Sorting.shuffleArray(arr);
        printArray(arr);
        System.out.println("Array ordenado: ");
        Sorting.mergeSort(arr);
        printArray(arr);
    }
    
    static void printArray(int[] arr) {
        System.out.print("[ ");
        for(int i = 0; i < arr.length-1; i++) {
            System.out.print("" + arr[i] + ", ");
        }
        System.out.print("" + arr[arr.length-1]);
        System.out.println(" ]");
        
    }
    
}
