/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheros;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Scanner;

/**
 *
 * @author Zhel-PC
 */
public class Writer {
        public static void main(String[] args) {    
        Scanner scan = new Scanner(System.in);
        BufferedWriter br;
        try {
            File f = new File("nombres.txt");
            FileWriter fr = new FileWriter(f);
            br = new BufferedWriter(fr);
            
            System.out.println("Escriba lo que quiere guardar en el archivo: ");
            String input = scan.nextLine();
            br.write(input);
            br.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
}
