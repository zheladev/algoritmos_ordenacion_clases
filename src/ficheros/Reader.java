/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheros;
import java.io.*;

/**
 *
 * @author Zhel-PC
 */
public class Reader {
    public static void main(String[] args) {
        try {
            File f = new File("nombres.txt");
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            System.out.println("Nombres en nombres.txt: ");
            System.out.println(f.getAbsolutePath());
            String line = br.readLine();
            while (line != null) {
                System.out.println(line);
                line = br.readLine();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
}
