/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheros;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Scanner;

/**
 *
 * @author Zhel-PC
 */
public class OutStream {
    public static void main(String[] args) throws Exception {
        Scanner scan = new Scanner(System.in);
        File f = new File("OutStream.txt");
        FileOutputStream fos = new FileOutputStream(f);
        System.out.println("Escriba cosas para meter en el archivo: ");
        String input = scan.nextLine();
        
        char[] inputArr = input.toCharArray();
        for(char c : inputArr) {
            fos.write(c);
        }
        
        fos.close();
    }
}
