/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ficheros;

import java.io.DataInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;

/**
 *
 * @author Zhel-PC
 */
public class InStream {
        public static void main(String[] args) throws Exception {
        File f = new File("inputStream.txt");
        FileInputStream fos = new FileInputStream(f);
        DataInputStream dis = new DataInputStream(fos);
        System.out.println("Datos contenidos en input.txt: ");
        String data = "";
        try {
            while(true) {
                data+=(char)dis.readByte();
            }
        } catch(EOFException e) {
            System.out.println(data);
        }
        
        fos.close();
    }
}
