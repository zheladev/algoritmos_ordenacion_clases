/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;
import java.lang.ArithmeticException;
/**
 *
 * @author Zhel-PC
 */
public class Calculadora {
    static double multiplicar(double num, double num2) {
        return num * num2;
    }
    
    static double dividir(double dividendo, double divisor) throws DividirPorCeroException {
        if (divisor == 0) {
            throw new DividirPorCeroException(dividendo);
        }
        
        return dividendo/divisor;
    }
}
