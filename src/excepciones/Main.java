/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author Zhel-PC
 */
public class Main {
    public static void main(String [] args) {
        double numero1 = 10, numero2 = 0;
        
        System.out.println(Calculadora.multiplicar(numero1, numero2));
        
        try {
            System.out.println(Calculadora.dividir(numero1, numero2));
        } catch(DividirPorCeroException e) {
            System.out.println(e.getMessage());
        }
        
    } 
}
