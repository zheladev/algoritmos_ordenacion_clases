/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author Zhel-PC
 */
public class DividirPorCeroException extends Exception {
    public DividirPorCeroException(double dividendo) {
        super("Se ha intentando dividir " + dividendo + " entre 0.");
    }
}
